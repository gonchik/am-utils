package ru.andreymarkelov.atlas.plugins;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.atlassian.jira.util.I18nHelper;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;
import org.apache.commons.lang3.StringUtils;
import ru.andreymarkelov.atlas.plugins.amutils.util.Utils;

import java.util.HashMap;
import java.util.Map;

public class JqlValidatorFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginValidatorFactory {
    public static final String JQL = "jqlstr";
    public static final String COUNT = "issuecount";
    public static final String ERROR = "error";
    public static final String ERRORVIEW = "errorview";
    public static final String EXCLUDEITSELF = "excludeitself";

    private final I18nHelper i18nHelper;

    public JqlValidatorFactory(I18nHelper i18nHelper) {
        this.i18nHelper = i18nHelper;
    }

    @Override
    public Map<String, ?> getDescriptorParams(Map<String, Object> conditionParams) {
        Map<String, Object> map = new HashMap<>();

        if (conditionParams != null && conditionParams.containsKey(JQL)) {
            map.put(JQL, extractSingleParam(conditionParams, JQL));
        } else {
            map.put(JQL, "");
        }

        if (conditionParams != null && conditionParams.containsKey(COUNT)) {
            String countStr = extractSingleParam(conditionParams, COUNT);
            map.put(COUNT, Utils.isPositiveInteger(countStr) ? Integer.valueOf(countStr) : Integer.valueOf(0));
        } else {
            map.put(COUNT, 0);
        }

        if (conditionParams != null && conditionParams.containsKey(ERROR)) {
            map.put(ERROR, extractSingleParam(conditionParams, ERROR));
        }

        if (conditionParams != null && conditionParams.containsKey(EXCLUDEITSELF)) {
            map.put(EXCLUDEITSELF, Boolean.TRUE);
        } else {
            map.put(EXCLUDEITSELF, Boolean.FALSE);
        }

        return map;
    }

    private String getParam(AbstractDescriptor descriptor, String param) {
        if (!(descriptor instanceof ValidatorDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor.");
        }

        ValidatorDescriptor validatorDescriptor = (ValidatorDescriptor) descriptor;
        String value = (String) validatorDescriptor.getArgs().get(param);

        return StringUtils.isNoneBlank(value) ? value : StringUtils.EMPTY;
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(JQL, getParam(descriptor, JQL));
        velocityParams.put(COUNT, getParam(descriptor, COUNT));
        velocityParams.put(ERROR, getParam(descriptor, ERROR));
        velocityParams.put(EXCLUDEITSELF, getParam(descriptor, EXCLUDEITSELF));
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put(JQL, "");
        velocityParams.put(COUNT, 0);
        velocityParams.put(ERROR, "");
        velocityParams.put(EXCLUDEITSELF, Boolean.TRUE);
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(JQL, getParam(descriptor, JQL));
        velocityParams.put(COUNT, getParam(descriptor, COUNT));
        velocityParams.put(EXCLUDEITSELF, getParam(descriptor, EXCLUDEITSELF));
        String error = getParam(descriptor, ERROR);
        velocityParams.put(ERRORVIEW, StringUtils.isNotBlank(error) ? String.format(error, "XXX-X: XXXXX") : i18nHelper.getText("ru.andreymarkelov.atlas.plugins.amutils.jcl-validator.error", "XXX-X", "XXXXX"));
    }
}

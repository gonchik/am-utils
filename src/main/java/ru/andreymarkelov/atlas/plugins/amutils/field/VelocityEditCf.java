package ru.andreymarkelov.atlas.plugins.amutils.field;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.security.roles.ProjectRoleManager;

import java.util.Collection;
import java.util.Map;

public class VelocityEditCf extends GenericTextCFType {
    private final ProjectRoleManager projectRoleManager;

    public VelocityEditCf(
            CustomFieldValuePersister customFieldValuePersister,
            GenericConfigManager genericConfigManager,
            ProjectRoleManager projectRoleManager) {
        super(customFieldValuePersister, genericConfigManager);
        this.projectRoleManager = projectRoleManager;
    }

    @Override
    public String getValueFromIssue(CustomField field, Issue issue) {
        FieldConfig config = field.getRelevantConfig(issue);
        if (config == null) {
            return null;
        } else {
            return super.getDefaultValue(config);
        }
    }

    @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField customField, FieldLayoutItem fieldLayoutItem) {
        Map<String, Object> params = super.getVelocityParameters(issue, customField, fieldLayoutItem);
        if (issue != null) {
            String value = null;
            FieldConfig config = customField.getRelevantConfig(issue);
            if (config != null) {
                value = super.getDefaultValue(config);
            }
            if (value != null) {
                params.put("value", value);
                params.put("defaultValue", value);
            }
        }
        params.put("VelocityManager", ComponentAccessor.getVelocityManager());
        params.put("projectRoleManager", projectRoleManager);
        return params;
    }

    @Override
    public void createValue(CustomField field, Issue issue, String value) {
    }

    @Override
    public void updateValue(CustomField customField, Issue issue, String value) {
    }

    @Override
    public String getChangelogValue(CustomField customField, String value) {
        return null;
    }

    @Override
    public String getChangelogString(CustomField customField, String value) {
        return null;
    }

    @Override
    public String getValueFromCustomFieldParams(CustomFieldParams relevantParams) throws FieldValidationException {
        Collection<?> values = relevantParams.getValuesForNullKey();
        if (!values.isEmpty()) {
            return (String) values.iterator().next();
        }
        return "";
    }
}


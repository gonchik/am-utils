package ru.andreymarkelov.atlas.plugins.amutils.functions;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleActors;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class MembersOfProjectRoleFunction extends AbstractJqlFunction {
    private final ProjectRoleManager projectRoleManager;
    private final ProjectManager projectManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public MembersOfProjectRoleFunction(
            ProjectRoleManager projectRoleManager,
            ProjectManager projectManager,
            JiraAuthenticationContext jiraAuthenticationContext) {
        this.projectRoleManager = projectRoleManager;
        this.projectManager = projectManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.USER;
    }

    @Override
    public String getFunctionName() {
        return "membersOfProjectRole";
    }

    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 2;
    }

    @Override
    public List<QueryLiteral> getValues(
            QueryCreationContext queryCreationContext,
            FunctionOperand functionOperand,
            TerminalClause terminalClause) {
        if (!functionOperand.getArgs().isEmpty()) {
            final String projectName = functionOperand.getArgs().get(0);
            final String projectRoleName = functionOperand.getArgs().get(1);

            ProjectRole projectRole = projectRoleManager.getProjectRole(projectRoleName);
            Project project = projectManager.getProjectObjByKeyIgnoreCase(projectName);
            ProjectRoleActors projectRoleActors = projectRoleManager.getProjectRoleActors(projectRole, project);

            final Set<QueryLiteral> usernames = new LinkedHashSet<>();
            Set<ApplicationUser> applicationUsers = projectRoleActors.getUsers();
            for(ApplicationUser applicationUser : applicationUsers) {
                usernames.add(new QueryLiteral(functionOperand, applicationUser.getName()));
            }
            return new ArrayList<>(usernames);
        }
        return Collections.emptyList();
    }

    @Override
    public boolean isList() {
        return true;
    }

    @Override
    public MessageSet validate(
            ApplicationUser applicationUser,
            FunctionOperand functionOperand,
            TerminalClause terminalClause) {
        MessageSet messages = new MessageSetImpl();
        messages.addMessageSet(validateNumberOfArgs(functionOperand, 2));
        if (messages.hasAnyErrors()) {
            return messages;
        }

        String projectName = functionOperand.getArgs().get(0);
        String projectRoleName = functionOperand.getArgs().get(1);

        ProjectRole projectRole = projectRoleManager.getProjectRole(projectRoleName);
        if (projectRole == null) {
            messages.addErrorMessage(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.functions.membersofprojectrole.error.projectrole"));
        }

        Project project = projectManager.getProjectObjByKeyIgnoreCase(projectName);
        if (project == null) {
            messages.addErrorMessage(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.functions.membersofprojectrole.error.project"));
        }

        return messages;
    }
}

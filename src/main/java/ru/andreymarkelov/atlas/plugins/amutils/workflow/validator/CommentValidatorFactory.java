package ru.andreymarkelov.atlas.plugins.amutils.workflow.validator;

import com.atlassian.jira.bc.group.search.GroupPickerSearchService;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginValidatorFactory;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ValidatorDescriptor;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class CommentValidatorFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginValidatorFactory {
    public static final String ALL_GROUPS = "all-groups";
    public static final String SELECTED_GROUPS = "selectedGroupsList";
    public static final String SELECTED_GROUPS_SET = "selectedGroupsListSet";
    public static final String ERROR = "error";

    private final GroupPickerSearchService groupPickerSearchService;

    public CommentValidatorFactory(GroupPickerSearchService groupPickerSearchService) {
        this.groupPickerSearchService = groupPickerSearchService;
    }

    public Map<String, Object> getDescriptorParams(Map<String, Object> validatorParams) {
        Map<String, Object> map = new HashMap<>();

        if (validatorParams != null && validatorParams.containsKey(SELECTED_GROUPS)) {
            map.put(SELECTED_GROUPS, extractSingleParam(validatorParams, SELECTED_GROUPS));
        } else {
            map.put(SELECTED_GROUPS, "");
        }

        if (validatorParams != null && validatorParams.containsKey(ERROR)) {
            map.put(ERROR, extractSingleParam(validatorParams, ERROR));
        } else {
            map.put(ERROR, "");
        }

        return map;
    }

    private String getParam(AbstractDescriptor descriptor, String param) {
        if (!(descriptor instanceof ValidatorDescriptor)) {
            throw new IllegalArgumentException("Descriptor must be a ValidatorDescriptor.");
        }

        String value = (String) ((ValidatorDescriptor) descriptor).getArgs().get(param);
        return StringUtils.isNotEmpty(value) ? value : "";
    }

    private Set<String> getSetParams(AbstractDescriptor descriptor, String param) {
        String[] tokens = StringUtils.split(getParam(descriptor, param), "&");
        return (tokens != null) ? new TreeSet<>(Arrays.asList(tokens)) : new TreeSet<>();
    }

    @Override
    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(ALL_GROUPS, groupPickerSearchService.findGroups(""));
        velocityParams.put(SELECTED_GROUPS_SET, getSetParams(descriptor, SELECTED_GROUPS));
        velocityParams.put(ERROR, getParam(descriptor, ERROR));
    }

    @Override
    protected void getVelocityParamsForInput(Map<String, Object> velocityParams) {
        velocityParams.put(ALL_GROUPS, groupPickerSearchService.findGroups(""));
        velocityParams.put(SELECTED_GROUPS_SET, new TreeSet<String>());
        velocityParams.put(ERROR, "");
    }

    @Override
    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor) {
        velocityParams.put(SELECTED_GROUPS, getSetParams(descriptor, SELECTED_GROUPS));
        velocityParams.put(ERROR, getParam(descriptor, ERROR));
    }
}

package ru.andreymarkelov.atlas.plugins.amutils.functions;

import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;

import javax.annotation.Nonnull;
import java.util.List;

public class IssueHasRegexVersions extends AbstractJqlFunction {
    @Nonnull
    @Override
    public MessageSet validate(
            ApplicationUser applicationUser,
            @Nonnull FunctionOperand functionOperand,
            @Nonnull TerminalClause terminalClause) {
        return null;
    }

    @Nonnull
    @Override
    public List<QueryLiteral> getValues(
            @Nonnull QueryCreationContext queryCreationContext,
            @Nonnull FunctionOperand functionOperand,
            @Nonnull TerminalClause terminalClause) {
        return null;
    }

    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 1;
    }

    @Nonnull
    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.VERSION;
    }
}

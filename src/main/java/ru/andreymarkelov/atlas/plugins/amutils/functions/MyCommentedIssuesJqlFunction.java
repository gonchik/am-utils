package ru.andreymarkelov.atlas.plugins.amutils.functions;

import com.atlassian.core.util.DateUtils;
import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.exception.DataAccessException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.atlassian.jira.ofbiz.DefaultOfBizConnectionFactory.getInstance;
import static org.apache.commons.lang3.StringUtils.lowerCase;

public class MyCommentedIssuesJqlFunction extends AbstractJqlFunction {
    private final static Log log = LogFactory.getLog(MyCommentedIssuesJqlFunction.class);

    private final static String SQL = "SELECT ISSUEID FROM jiraaction WHERE ACTIONTYPE = 'comment' AND UPDATED > ? AND UPDATEAUTHOR = ? ORDER BY UPDATED DESC";

    private final PermissionManager permissionManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final IssueManager issueManager;

    public MyCommentedIssuesJqlFunction(
            PermissionManager permissionManager,
            JiraAuthenticationContext jiraAuthenticationContext,
            IssueManager issueManager) {
        this.permissionManager = permissionManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.issueManager = issueManager;
    }

    @Override
    public JiraDataType getDataType() {
        return JiraDataTypes.ISSUE;
    }

    @Override
    public int getMinimumNumberOfExpectedArguments() {
        return 1;
    }

    @Override
    public List<QueryLiteral> getValues(QueryCreationContext context, FunctionOperand operand, TerminalClause terminalClause) {
        if (operand.getArgs().size() != 1) {
            return null;
        }
        String time = operand.getArgs().get(0);

        long lastFindTime = System.currentTimeMillis();
        if ("startOfWeek".equals(time)) {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.DAY_OF_WEEK, 1);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.clear(Calendar.MINUTE);
            cal.clear(Calendar.SECOND);
            cal.clear(Calendar.MILLISECOND);
            lastFindTime = cal.getTimeInMillis();
        } else if ("startOfDay".equals(time)) {
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.clear(Calendar.MINUTE);
            cal.clear(Calendar.SECOND);
            cal.clear(Calendar.MILLISECOND);
            lastFindTime = cal.getTimeInMillis();
        } else {
            try {
                long diffTime = DateUtils.getDuration(time);
                lastFindTime -= (diffTime * 1000);
            } catch (InvalidDurationException e) {
                return null;
            }
        }

        if (!jiraAuthenticationContext.isLoggedInUser()) {
            return null;
        }

        List<QueryLiteral> literals = new ArrayList<>();
        try (Connection connection  = getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL)) {
            preparedStatement.setTimestamp(1, new Timestamp(lastFindTime));
            preparedStatement.setString(2, lowerCase(jiraAuthenticationContext.getLoggedInUser().getKey()));
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Long issueId = resultSet.getLong(1);
                    Issue issue = issueManager.getIssueObject(issueId);
                    if (issue != null && permissionManager.hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, context.getApplicationUser())) {
                        literals.add(new QueryLiteral(operand, issueId));
                    }
                }
            }
        } catch (DataAccessException|SQLException e) {
            log.error("MyCommentedIssuesJqlFunction::getValues - An error occured", e);
            return null;
        }
        return literals;
    }

    @Override
    public MessageSet validate(ApplicationUser searcher, FunctionOperand operand, TerminalClause terminalClause) {
        MessageSet messages = new MessageSetImpl();
        List<String> keys = operand.getArgs();
        if (keys.size() != 1) {
            messages.addErrorMessage(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.jql.commentedissues.incorrectparameters", "commentedIssues"));
        } else {
            String time = keys.get(0);
            if ("startOfWeek".equals(time) || "startOfDay".equals(time)) {
                //--> nothing
            } else {
                try {
                    DateUtils.getDuration(time);
                    if (!jiraAuthenticationContext.isLoggedInUser()) {
                        messages.addErrorMessage(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.jql.commentedissues.notloggeduser", "commentedIssues"));
                    }
                } catch (InvalidDurationException e) {
                    messages.addErrorMessage(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.utils.jql.commentedissues.incorrectparameters", "commentedIssues"));
                }
            }
        }
        return messages;
    }
}

package ru.andreymarkelov.atlas.plugins.amutils.field;

import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.impl.GenericTextCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.JiraDurationUtils;

import static org.apache.commons.lang3.StringUtils.defaultString;

public class EstimateValueCf extends GenericTextCFType {
    private final JiraDurationUtils jiraDurationUtils;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public EstimateValueCf(
            CustomFieldValuePersister customFieldValuePersister,
            GenericConfigManager genericConfigManager,
            JiraAuthenticationContext jiraAuthenticationContext) {
        super(customFieldValuePersister, genericConfigManager);
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.jiraDurationUtils = ComponentAccessor.getComponentOfType(JiraDurationUtils.class);
    }

    public String getSingularObjectFromString(String value) throws FieldValidationException {
        final String estimateTime = defaultString(value).trim();
        try {
            jiraDurationUtils.parseDuration(estimateTime, jiraAuthenticationContext.getLocale());
        } catch (InvalidDurationException e) {
            throw new FieldValidationException(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.field.estimate.error.format"));
        }
        return estimateTime;
    }
}

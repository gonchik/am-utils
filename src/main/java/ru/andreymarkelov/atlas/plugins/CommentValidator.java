package ru.andreymarkelov.atlas.plugins;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.user.ApplicationUser;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import org.apache.commons.lang3.StringUtils;
import ru.andreymarkelov.atlas.plugins.amutils.workflow.validator.CommentValidatorFactory;

import java.util.Map;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class CommentValidator implements Validator {
    private final GroupManager groupManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public CommentValidator(GroupManager groupManager, JiraAuthenticationContext jiraAuthenticationContext) {
        this.groupManager = groupManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void validate(Map transientVars, Map args, PropertySet ps) throws InvalidInputException, WorkflowException {
        String comment = (String) transientVars.get("comment");
        if (isNotEmpty(comment)) {
            return;
        }

        String selectedGroupsListSet = (String) args.get(CommentValidatorFactory.SELECTED_GROUPS);
        String error = (String) args.get(CommentValidatorFactory.ERROR);

        ApplicationUser user = jiraAuthenticationContext.getLoggedInUser();
        String[] selectedGroups = StringUtils.split(selectedGroupsListSet, "&");
        if (selectedGroups != null) {
            for (String selectedGroup : selectedGroups) {
                if (groupManager.getGroupNamesForUser(user).contains(selectedGroup)) {
                    return;
                }
            }
        }

        if (isNotEmpty(error)) {
            throw new InvalidInputException(error);
        } else {
            throw new InvalidInputException(jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.comment-validator.error"));
        }
    }
}

package ru.andreymarkelov.atlas.plugins;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isEmpty;

public class RegexValidator implements Validator {
    private final Logger log = Logger.getLogger(RegexValidator.class);

    private final CustomFieldManager customFieldManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public RegexValidator(
            CustomFieldManager customFieldManager,
            JiraAuthenticationContext jiraAuthenticationContext) {
        this.customFieldManager = customFieldManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public void validate(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        Issue issue = (Issue) transientVars.get("issue");
        String customFieldId = (String) args.get("cfId");
        String regex = (String) args.get("regex");
        String errorMessage = (String) args.get("msg");

        if (isBlank(customFieldId) || isBlank(regex)) {
            log.warn("CustomFieldId or Regex is not set.");
            return;
        }

        Pattern pattern;
        try {
            pattern = Pattern.compile(regex);
        } catch (Exception ex) {
            log.warn("Regex is invalid", ex);
            return;
        }

        String errorMsg;
        if (isEmpty(errorMessage)) {
            errorMsg = jiraAuthenticationContext.getI18nHelper().getText("ru.andreymarkelov.atlas.plugins.amutils.regex-validator.error", regex);
        } else {
            errorMsg = String.format(errorMessage, regex);
        }

        CustomField customField = customFieldManager.getCustomFieldObject(customFieldId);
        if (customField != null) {
            String customFieldValue = formatCustomFieldValue(issue.getCustomFieldValue(customField));
            if (customFieldValue == null || !pattern.matcher(customFieldValue.toString()).matches()) {
                throw new InvalidInputException(errorMsg);
            }
        }
    }

    private static String formatCustomFieldValue(Object customFieldValue) {
        if (customFieldValue == null) {
            return null;
        }

        if (customFieldValue instanceof Double) {
            double aDouble = (Double) customFieldValue;
            return (aDouble == (long) aDouble) ? String.format("%d",(long) aDouble) : String.format("%s", aDouble);
        } else {
            return customFieldValue.toString();
        }
    }
}

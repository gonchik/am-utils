package ru.andreymarkelov.atlas.plugins;

public interface Consts {
    String ALL_STATUSES = "statuses";

    String SELECTED_STATUS = "selectedStatus";

    String ISSUE_PROJECT = "issueProject";

    String ISSUE_TYPE = "issueType";

    String ISSUE_STATUS = "issueStatus";

    String CLONE_PREFIX = "clonePrefix";

    String CLONE_ASSIGNEE = "cloneAssignee";

    String CLONE_LINK_TYPE = "cloneLink";

    /**
     * Issue clone count.
     */
    String ISSUE_CLONE_COUNT = "issueCloneCount";

    /**
     * Is clone with attachments?
     */
    String ISSUE_CLONE_ATTACHMENTS = "isCloneWithAttchments";

    /**
     * Is clone with links?
     */
    String ISSUE_CLONE_LINKS = "isCloneWithLinks";
}

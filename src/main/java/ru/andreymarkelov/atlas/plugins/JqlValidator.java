package ru.andreymarkelov.atlas.plugins;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.jql.parser.DefaultJqlQueryParser;
import com.atlassian.jira.jql.parser.JqlParseException;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.query.Query;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.InvalidInputException;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.WorkflowException;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.Objects;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.replace;

public class JqlValidator implements Validator {
    private final Logger log = Logger.getLogger(JqlValidator.class);

    private static final String SUBSTITUTION_TOKEN = "%THIS%";

    private final SearchService searchService;
    private final I18nHelper i18nHelper;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public JqlValidator(SearchService searchService, I18nHelper i18nHelper, JiraAuthenticationContext jiraAuthenticationContext) {
        this.searchService = searchService;
        this.i18nHelper = i18nHelper;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public void validate(Map transientVars, Map args, PropertySet ps) throws WorkflowException {
        Issue issue = (Issue) transientVars.get("issue");
        String jql = Objects.toString(args.get(JqlValidatorFactory.JQL));
        int count = NumberUtils.toInt(Objects.toString(args.get(JqlValidatorFactory.COUNT), "0"));
        boolean excludeItself = BooleanUtils.toBoolean(Objects.toString(args.get(JqlValidatorFactory.EXCLUDEITSELF), "true"));
        String error = Objects.toString(args.get(JqlValidatorFactory.ERROR));

        if (!isNotBlank(jql)) {
            log.warn("JqlValidator::validate - No query configured");
            return;
        } else {
            jql = replace(jql, SUBSTITUTION_TOKEN, issue.getKey());
        }

        if (!jiraAuthenticationContext.isLoggedInUser()) {
            log.warn("JqlValidator::validate - User is not logged");
            return;
        }

        try {
            Query query = new DefaultJqlQueryParser().parseQuery(jql);
            if (excludeItself && issue.getKey() != null) {
                JqlQueryBuilder builder = JqlQueryBuilder.newBuilder();
                builder.where().addClause(query.getWhereClause()).and().not().issue(issue.getKey());
                builder.orderBy().setSorts(query.getOrderByClause().getSearchSorts());
                query = builder.buildQuery();
            }
            SearchResults results = searchService.search(jiraAuthenticationContext.getLoggedInUser(), query, PagerFilter.getUnlimitedFilter());
            if (results != null && results.getIssues().size() > count) {
                Issue i = results.getIssues().get(0);
                String message;
                if (!isNotBlank(error)) {
                    message = i18nHelper.getText("ru.andreymarkelov.atlas.plugins.amutils.jcl-validator.error", i.getKey(), i.getSummary());
                } else {
                    message = String.format(error, i.getKey().concat(": ").concat(i.getSummary()));
                }
                throw new InvalidInputException(message);
            }
        } catch (JqlParseException ex) {
            log.error(String.format("JqlValidator::validate - Error parse query \"%s\" with error \"%s\"",jql, ex.getMessage()));
        } catch (SearchException ex) {
            log.error(String.format("JqlValidator::validate - Error search \"%s\" with error \"%s\"",jql, ex.getMessage()));
        }
    }
}
